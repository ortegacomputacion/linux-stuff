
# Archlinux quick and dirty install recipe (legacy bios)

1. bootear el iso de arch
2. particionar el disco, (asumo sda, reemplazar si es necesario): cfdisk /dev/sda
  usar particion gpt disco completo, o 2 particions main y swap
3. formatear `mkfs.ext4 /dev/sda1`
4. montar `mount /dev/sda1 /mnt`
5. instalr base `pacstrap /mnt base linux linux-firmware`
6. genrar fstab `genfstab -U /mnt >> /mnt/etc/fstab`
7. chroot `arch-chroot /mnt`
8. bootlader y nano
- `pacman -S grub nano`
- `grub-install /dev/sda`
- `grub-mkconfig -o /boot/grub/grub.cfg`
9. pass de root con `passwd`
10. crear y editar `nano /etc/systemd/network/20-wired.network`
`[Match]
Name=<device name>
[Network]
DHCP=yes`
11. salir del chroot con ctrl-d
12. desmontar, `umount /mnt`
13. `reboot`
14. enjoy
